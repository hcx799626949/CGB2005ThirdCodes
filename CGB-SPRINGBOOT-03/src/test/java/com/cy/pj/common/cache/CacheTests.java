package com.cy.pj.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CacheTests {
    //@Autowired注解描述属性时,系统底层会基于属性类型从spring容器查找对象,假如有多个
	//类型都满足注入要求,则还会基于属性名进行查找,检测哪个bean名字与属性名相同,
	//假如有相同的则直接取相同进行注入,没有则抛出异常.
	@Autowired
	//@Qualifier注解配合@Autowired注解使用,
	//用于指定将哪个bean的名字对应的对象注入给描述的属性(例如cache属性)
	@Qualifier("weakCache")
	private Cache cache;
	
	@Test
	void testCache() {
		System.out.println("cache="+cache);
	}
}
