package com.cy.pj.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.cy.pj.common.cache.Cache;

@Component
public class SearchService {
	private Cache cache;
	
	@Autowired
	public SearchService( @Qualifier("softCache") Cache cache) {
		this.cache=cache;
		System.out.println("SearchService.this.cache="+this.cache);
	}
}
