package com.cy.pj.goods.dao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
/**
   * 通过如下类,演示GoodsDao接口对应的实现类大概的写法.
 */
//@Component
@Repository
public class GoodsDaoImpl {//GoodsDaoImplTests
	@Autowired
	private SqlSession sqlSession;//SqlSessionTemplate (线程安全)
	
	public int deleteObjects(int...ids) {
		String statement="com.cy.pj.goods.dao.GoodsDao.deleteObjects";
		return sqlSession.delete(statement,ids);
		//Map<String,Object> map=new HashMap<>();
		//map.put("ids", ids);
		//return sqlSession.delete(statement,map);
	}
	public int deleteById(Integer id) {
		//String namespace="com.cy.pj.goods.dao.GoodsDao";
		//String elementId="deleteById";
		//String statement=namespace+"."+elementId;
		String statement="com.cy.pj.goods.dao.GoodsDao.deleteById";
		return sqlSession.delete(statement,id);
	}
}
