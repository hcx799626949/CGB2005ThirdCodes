package com.cy.pj.goods.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cy.pj.goods.pojo.Goods;

/**
   * 封装封装商品数据逻辑的数据访问对象.
 * @Mapper 注解由mybatis提供,主要用于描述
  *    数据层接口对象,系统底层会基于此注解的描述
  *   为接口创建其实现类对象,然后将此对象交给spring
  *   管理.
 */
@Mapper
public interface GoodsDao {//com.cy.pj.goods.dao.GoodsDao
	
	
	@Select("select * from tb_goods where id=#{id}")
	Goods findById(Integer id);
	
	
	@Update("update tb_goods set name=#{name},remark=#{remark} where id=#{id}")
	int updateObject(Goods entity);
	
	//@Insert("insert into tb_goods(name,remark,createdTime) values (#{name},#{remark},now())")
	int insertObject(Goods entity);
	
	/**
	  * 查询所有商品信息,一行记录映射为一个Goods对象,多个对象存储到List集合.
	 * 1)底层查询还是JDBC
	 * 2)查询结果的映射(MyBatis框架):假如是JDBC我们需要获得结果ResultSet,然后取数据手动进行映射
	 *   内容中映射过程
	 * 1)迭代ResultSet(这个对象中封装了数据库中的查询结果)
	 * 2)构建Goods对象(迭代一次构建一个Goods对象)
	 * 3)从ResultSet中取值然后通过反射调用Goods对象set方法或者直接为属性赋值
	 */
	@Select("select * from tb_goods")
	List<Goods> findGoods();
	
	/**
	 * 基于多个商品id执行删除操作
	 * @param ids
	 * @return
	 */
	int deleteObjects(/*@Param("ids")*/Integer... ids);
    /**
            * 基于商品id执行删除业务
     * @param id 商品id
     * @return 删除的行数
     * 	数据层方法对象的sql映射,简单sql可以以注解方式进行定义,负责sql写到映射文件(xml)
     */
	@Delete("delete from tb_goods where id=#{id}")
	int deleteById(Integer id);
}



