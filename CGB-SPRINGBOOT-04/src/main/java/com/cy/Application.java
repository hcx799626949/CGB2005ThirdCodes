package com.cy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cy.pj.goods.dao.GoodsDao;
//@MapperScan("com.cy.pj.goods.dao")//扫描指定包下的dao接口,接口上可以没有@Mapper注解
@SpringBootApplication
public class Application implements CommandLineRunner{
	@Autowired
	private GoodsDao goodsDao;
	/**
	 * 假如我们在Springboot工程项目中,需要在项目启动以后做点事情,可以
	 * 让你的启动类实现CommandLineRunner接口,重写run方法,在此方法中
	 * 做点事情.
	 */
	@Override
	public void run(String... args) throws Exception {
		//int rows=goodsDao.deleteById(9);
		//System.out.println("delete.rows="+rows);
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
