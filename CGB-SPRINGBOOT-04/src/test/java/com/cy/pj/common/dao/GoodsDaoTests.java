package com.cy.pj.common.dao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.cy.pj.goods.dao.GoodsDao;

@SpringBootTest
public class GoodsDaoTests {

	@Autowired
	private GoodsDao goodsDao;//思考:这个变量指向的对象是谁,什么类型的?
	
	@Test
	void testDeleteObjects() {
		//当我们在执行此方法时,其实现类内部会检测接口方法上是否有定义sql映射.
		//假如没有,然后基于接口类全名(包名.类名)找到对应的映射文件,
		//然后再基于方法名找到对应映射文件中的元素,进而获取SQL映射
		int rows=goodsDao.deleteObjects(10,20);
		System.out.println("delete.rows="+rows);
	}
	
	@Test
	void testDeleteById() {
		System.out.println(goodsDao.getClass().getName());//com.sun.proxy.$Proxy55
		int rows=goodsDao.deleteById(10);//思考:底层在做什么?(基于sqlSession与数据库会话)
		System.out.println("delete.rows="+rows);
	}
	
}
