package com.cy.pj.common.datasource;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * 整合hikarCP连接池
 *1)创建SpringBoot项目
 *2)添加依赖(两个-MySQL 驱动,Spring-JDBC)
 *3)配置(application.properties)连接数据库的url,username,password
 *3.1)spring.datasource.url=jdbc:mysql:///dbgoods?serverTimezone=GMT%2B8&characterEncoding=utf8
 *3.2)spring.datasource.username=root
 *3.3)spring.datasource.password=root
 *4)编写测试类DataSourceTest从池中获取连接(Connection)
 */
@SpringBootTest
public class DataSourceTests {//Test.class
	//思考:
	//1)这里dataSource变量指向的对象是谁?你怎么知道的?
	@Autowired
	private DataSource dataSource;//HikariDataSource
	@Test //Test.class
	void testConnection01() throws SQLException {
		//获取dataSource变量指向的对象的具体类型的类全名
		//System.out.println(dataSource.getClass().getName());
		//思考:这个连接的获取过程是怎样的?你怎么知道的?
		System.out.println(dataSource.getConnection());
	}
	@Test //Test.class
	void testConnection02() throws Exception {
		Connection conn1=dataSource.getConnection();
//		System.out.println("conn1="+conn1);
//        Connection conn2=dataSource.getConnection();
		//System.out.println("conn2="+conn2);
//		conn1.close();
//		conn2.close();
//		Thread.sleep(2000);
//   	Connection conn3=dataSource.getConnection();
//		System.out.println("conn3="+conn3);
	}
}



