package com.cy.pj.common.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.cy.pj.goods.dao.GoodsDaoImpl;

@SpringBootTest
public class GoodsDaoImplTests {
  
	@Autowired
	private GoodsDaoImpl goodsDaoImpl;
	
	@Test
	void testDeleteObjects() {
		int rows=goodsDaoImpl.deleteObjects(6,7,8);
		System.out.println("delete.rows="+rows);
	}
	
	@Test
	void testDeleteById() {
		int rows=goodsDaoImpl.deleteById(8);
		System.out.println("delete.rows="+rows);
	}
}


