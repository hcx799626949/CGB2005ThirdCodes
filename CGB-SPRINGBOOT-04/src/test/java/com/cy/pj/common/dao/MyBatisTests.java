package com.cy.pj.common.dao;

import java.sql.Connection;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
//测试mybatis环境
@SpringBootTest
public class MyBatisTests {
	 /**
	  * MyBatis中的会话工厂(Springboot底层会进行工厂的自动配置),这个工厂对象
	     * 由Spring框架创建及管理.
	  */
	 @Autowired
	 private SqlSessionFactory sqlSessionFactory;
	 @Test
	 void testSqlSessionFactory() {
		 //获取具体会话工厂类型,并输出.
		 //org.apache.ibatis.session.defaults.DefaultSqlSessionFactory
		 System.out.println(sqlSessionFactory.getClass().getName());
	 }
	 @Test
	 void testSqlSession() {
		 //创建一个会话对象,并获取其具体类型,然后进行输出
		 SqlSession session=sqlSessionFactory.openSession();//此时会建立连接吗
		 System.out.println(session.getClass().getName());
	 }
	 @Test
	 void testGetConnection() {
		 SqlSession session=sqlSessionFactory.openSession();
		 Connection conn=session.getConnection();//DataSource/HikariDataSource/HikariPool/ConcurrentBag/...
		 System.out.println("conn="+conn);
	 }
	 
}





