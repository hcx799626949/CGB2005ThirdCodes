package com.cy.pj.goods.service;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import com.cy.pj.goods.pojo.Goods;

@SpringBootTest
public class GoodsServiceTests {

	@Autowired
	private GoodsService goodsService;
	
	@Test
	void testSaveGoods() {
		Goods g=new Goods();
		g.setName("JDBC");
		g.setRemark("Java.sql");
		goodsService.saveObject(g);
		System.out.println("insert ok");
	}
	
	@Test
	void testFindGoods() {
		List<Goods> list=goodsService.findGoods();
		for(Goods g:list) {
			System.out.println(g);
		}
	}
	
	@Test
	void testDeleteById() {
		 int rows=goodsService.deleteById(5);
		 System.out.println("delete.rows="+rows);
	}
}
