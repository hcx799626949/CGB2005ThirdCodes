package com.cy.pj.sys.util;

import java.security.MessageDigest;
import java.util.UUID;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.DigestUtils;

@SpringBootTest
public class MD5Tests {

	@Test
	void testMD501() {
		String pwd="123456";
		//String salt=UUID.randomUUID().toString();//da5bf161-32eb-4521-970e-efe61d56a8e6
		String salt="da5bf161-32eb-4521-970e-efe61d56a8e6";
		System.out.println("salt="+salt);
		String newPassword=DigestUtils.md5DigestAsHex(pwd.getBytes());//Spring
		System.out.println("newPassword="+newPassword);//e10adc3949ba59abbe56e057f20f883e
		String newSaltPassword=DigestUtils.md5DigestAsHex((salt+pwd).getBytes());
		System.out.println("newSaltPassword="+newSaltPassword);//e17c3f5684d5d4b2e59f8d46f8a486e5
	}
	
	@Test
	void testMD502() {
		String pwd="123456";
		String salt="da5bf161-32eb-4521-970e-efe61d56a8e6";
		SimpleHash sh=new SimpleHash("MD5", pwd, salt, 1);//Shiro
		String newSaltPassword=sh.toHex();//将加密结果转换为16进制字符串
		System.out.println(newSaltPassword.length());
		System.out.println("newSaltPassword="+newSaltPassword);//e17c3f5684d5d4b2e59f8d46f8a486e5
	}
	
	@Test
	void testMD503()throws Exception {//基于JDK原生API对字符串进行MD5加密(了解)
		String pwd="123456";
		MessageDigest md=MessageDigest.getInstance("MD5");
		byte[]digest=md.digest(pwd.getBytes());
		System.out.println(digest.length);//16byte,128bit
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<digest.length;i++) {
			String hexStr=Integer.toHexString(digest[i]&0xFF);
			System.out.println(hexStr);
			if(hexStr.length()==1) {
				hexStr='0'+hexStr;
			}
			sb.append(hexStr);
		}
		System.out.println(sb.toString());
	}
	
	
	
	
}
