package com.cy.pj.sys.dao;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.cy.pj.sys.pojo.SysLog;

@SpringBootTest
public class SysLogDaoTests {

	@Autowired
	private SysLogDao sysLogDao;
	
	@Test
	void testFindPageObjects() {
		List<SysLog> list=sysLogDao.findPageObjects("admin",0L, 5);
		list.forEach(item->System.out.println(item));//->为jdk8中的箭头符号
	}
	
	@Test
	void testGetRowCount() {
		//long rowCount=sysLogDao.getRowCount("admin");
		long rowCount=sysLogDao.getRowCount(null);
		System.out.println("rowCount="+rowCount);
	}
}
