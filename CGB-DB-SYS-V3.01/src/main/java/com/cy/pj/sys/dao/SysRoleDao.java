package com.cy.pj.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.pojo.SysRoleMenu;

@Mapper
public interface SysRoleDao {
	/**
	 * 	查询所有角色的角色id和名字
	 * @return
	 */
	@Select("select id,name from sys_roles")
	List<CheckBox> findRoles();
	
	/**
	 * 	基于id查询角色自身信息(id,name,note)
	 * @param id
	 * @return
	 */
	SysRoleMenu findById(Integer id);
	
	/**
	 * 	更新角色自身信息
	 * @param entity
	 * @return
	 */
	int updateObject(SysRole entity);
	/**
	 * 	保存角色自身信息
	 * @param entity
	 * @return
	 */
	int insertObject(SysRole entity);
	 /**
	  * 	基于角色名统计角色相关信息
	  * @param name
	  * @return
	  */
	 long getRowCount(String name);
	 /**
	  *      查询当前页的角色信息
	  * @param name
	  * @param startIndex 起始位置 (下标默认从0开始)
	  * @param pageSize (页面大小,每页最多显示多少条记录)
	  * @return 返回查询到的当前页记录
	  */
	 List<SysRole> findPageObjects(String name,Long startIndex,Integer pageSize);
	 
}




