package com.cy.pj.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.pojo.SysUserMenu;

/**
 * 	操作菜单表数据的一个数据层对象
 * @author Administrator
 */
@Mapper
public interface SysMenuDao {
	/**
	 *	 基于给点的菜单查询1级和2级菜单
	 * @param menuIds (这个id的获取首先需要获得登录用户,然后通过登录用户获取用户对应的菜单)
	 * @return
	 */
	List<SysUserMenu> findMenusByIds(List<Integer> menuIds);
	
	 /**
	  * 	查询多个菜单id对应的权限标识(permission)
	  * @param menuIds
	  * @return
	  */
	 List<String> findPermissions(List<Integer> menuIds);
	
	 int updateObject(SysMenu entity);
	
	 int insertObject(SysMenu entity);
	  /**
	   * 	获取所有菜单的id,name,parentId,在客户端会基于这些数据呈现树结构.
	   * @return
	   */
	  @Select("select id,name,parentId from sys_menus")
	  List<Node> findZtreeMenuNodes();

	
	  /**
	   * 	基于id统计子元素个数
	   * @param id
	   * @return
	   */
	  @Select("select count(*) from sys_menus where parentId=#{id}")
	  int getChildCount(Integer id);
	
	  /**
	   * 	基于id删除菜单元素
	   * @param id
	   * @return
	   */
	  @Delete("delete from sys_menus where id=#{id}")
	  int deleteObject(Integer id);
	  
      /**
                     查询表中所有菜单信息以及这个菜单对应的上级菜单信息,一行记录映射为内存中的一个Map对象,map中的key为表中字段名,值为表中字段对应的值?
        FAQ:
        1)在内存中使用map对象封装从数据库查询到的数据有什么优势,劣势?
        a)优势:灵活,开发速度快
        b)劣势:可读性比较差(打开源码不知道存了什么),值的类型不可控
        c)应用:实际项目中外包项目经常会用map封装数据,公司产品级应用(要求比较高)能用pojo则用pojo.
        2)为什么一行记录映射为一个map对象呢?(key相同值会覆盖)
       * @return
       */
	  List<Map<String,Object>> findObjects();
	  
	   
}







