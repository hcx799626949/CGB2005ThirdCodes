package com.cy.pj.sys.dao;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.cy.pj.sys.pojo.SysLog;

@Mapper
public interface SysLogDao {//其实现类为$Proxy35
	
	   int insertObject(SysLog entity);
	
	   int deleteObjects(Integer...ids);
       
	   long getRowCount(String username);
	   
	   List<SysLog> findPageObjects(String username,
			                        Long startIndex,
			                        Integer pageSize);
}
