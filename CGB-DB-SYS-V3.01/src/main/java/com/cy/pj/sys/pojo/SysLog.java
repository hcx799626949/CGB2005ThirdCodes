package com.cy.pj.sys.pojo;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
/**
 * 	定义用于封装日志信息的一个pojo对象,这样的对象,在定义时需要遵循以下规则:
 * 1)属性尽量都用对象类型
 * 2)提供无参数构造函数
 * 3)提供set/get/toString()方法,boolean类型变量不能以is作为前缀.
 * 4)实现序列化接口并手动添加序列化id(便于后续对此对象进行序列化):
 *          在java中建议所有用于存储数据的对象都实现序列化接口
 * FAQ?
 * 1)为什么要实现序列化接口?(便于序列化)
 * 2)什么是序列化?(将对象转换为字节)
 * 3)为什么要序列化?它应用在什么场景?(将对象钝化-持久存储或者存储到缓存,经过网络进行数据传递)
 * 4)什么是反序列化?(将字节再转换为对象)
 * 5)如何序列化和反序列化?
 * a)第一步:设计类时要实现序列化接口
 * b)第二步:构建I/O对象(ObjectOutputStream/ObjectInputStream)
 * c)第三步:通过I/O对象进行序列化和反序列化
 * 6)....................................
 */
@Data
public class SysLog implements Serializable{
	private static final long serialVersionUID = -1080110740434893535L;
	/**日志记录id*/
	private Long id;
	/**用户名(谁的操作日志-这个用户为操作这个系统的登陆用户)*/
	private String username;
	/**ip地址*/
	private String ip;
	/**用户执行的这个操作的操作名*/
	private String operation;
	/**记录用户访问的方法的全限定名(包名+类名+方法)*/
	private String method;
	/**记录用户访问方法时传入的参数*/
	private String params;
	/**用户访问方法时的执行时长*/
	private Long time;
	/**记录这条记录的创建时间*/
	private Date createdTime;
	//...
}
