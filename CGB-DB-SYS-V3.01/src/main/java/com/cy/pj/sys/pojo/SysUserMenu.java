package com.cy.pj.sys.pojo;

import java.util.List;

import lombok.Data;

@Data
public class SysUserMenu {

	private Integer id;
	private String name;
	private String url;
	private List<SysUserMenu> childs;
}
