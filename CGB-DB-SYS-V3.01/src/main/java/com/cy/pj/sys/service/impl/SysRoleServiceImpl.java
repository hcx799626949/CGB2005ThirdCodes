package com.cy.pj.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.dao.SysRoleDao;
import com.cy.pj.sys.dao.SysRoleMenuDao;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.pojo.SysRoleMenu;
import com.cy.pj.sys.service.SysRoleService;

@Service
public class SysRoleServiceImpl implements SysRoleService {

	@Autowired
	private SysRoleDao sysRoleDao;
	
	@Autowired
	private SysRoleMenuDao sysRoleMenuDao;
	
	@Override
	public List<CheckBox> findRoles() {
		// TODO Auto-generated method stub
		return sysRoleDao.findRoles();
	}
	
	//业务:基于id获取角色以及角色对应的菜单id,方案2实现(业务层一次查询,数据层sql嵌套多次查询)
	@Override
	public SysRoleMenu findById(Integer id) {
		//1.参数校验
		//2.基于id查询角色自身信息
		SysRoleMenu rm=sysRoleDao.findById(id);
		System.out.println("rm="+rm);
		if(rm==null)
			throw new ServiceException("没有找到对应记录");
		return rm;
	}
//业务:基于id获取角色以及角色对应的菜单id,方案1实现(业务层发起多次单表查询,然后实现统一封装)
//	@Override
//	public SysRoleMenu findById(Integer id) {
//		//1.参数校验
//		//2.基于id查询角色自身信息
//		SysRoleMenu rm=sysRoleDao.findById(id);//sys_roles
//	    if(rm==null)
//		throw new ServiceException("没有找到对应记录");
//		//3.基于id查询角色对应的菜单id
//		List<Integer> menuIds=sysRoleMenuDao.findMenuIdsByRoleId(id);//sys_role_menus
//		//4.封装查询结果并返回
//		rm.setMenuIds(menuIds);
//		return rm;
//	}
	
	@Override
	public int updateObject(SysRole entity, Integer[] menuIds) {
		//1.参数校验
		if(entity==null)
			throw new IllegalArgumentException("保存对象不能为空");
		if(StringUtils.isEmpty(entity.getName()))
			throw new IllegalArgumentException("角色名不能为空");
		if(menuIds==null||menuIds.length==0)
			throw new IllegalArgumentException("需要为角色指定权限");
		//2.更新角色自身信息
		int rows=sysRoleDao.updateObject(entity);
		if(rows==0)
			throw new ServiceException("记录可能已经不存在");
		//3.更新角色菜单关系数据
		//3.1删除原有关系数据
		sysRoleMenuDao.deleteObjectByRoleId(entity.getId());
		//3.2添加新的关系数据
		sysRoleMenuDao.insertObjects(entity.getId(), menuIds);
		//4.返回结果
		return rows;
	}
	@Override
	public int saveObject(SysRole entity, Integer[] menuIds) {
		//1.参数校验
		if(entity==null)
			throw new IllegalArgumentException("保存对象不能为空");
		if(StringUtils.isEmpty(entity.getName()))
			throw new IllegalArgumentException("角色名不能为空");
		if(menuIds==null||menuIds.length==0)
			throw new IllegalArgumentException("需要为角色指定权限");
		//2.保存角色自身信息
		int rows=sysRoleDao.insertObject(entity);
		//3.保存角色菜单关系数据
		sysRoleMenuDao.insertObjects(entity.getId(), menuIds);
		//4.返回结果
		return rows;
	}
	
	@Override
	public PageObject<SysRole> findPageObjects(String name, Long pageCurrent) {
		//1.参数校验
		//2.统计总记录数
		long rowCount=sysRoleDao.getRowCount(name);
		if(rowCount==0)throw new ServiceException("记录不存在");
		//3.查询当前页记录并进行封装
		int pageSize=2;
		long startIndex=(pageCurrent-1)*pageSize;
		List<SysRole> records=sysRoleDao.findPageObjects(name, startIndex, pageSize);
		return new PageObject<>(rowCount, records, pageSize, pageCurrent);
	}

}
