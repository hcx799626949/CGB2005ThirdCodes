package com.cy.pj.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface SysRoleMenuDao {
	
	@Select("select menu_id from sys_role_menus where role_id=#{roleId}")
	List<Integer> findMenuIdsByRoleId(Integer roleId);
	
	/**基于多个角色id获取对应的菜单id*/
	List<Integer> findMenuIdsByRoleIds(List<Integer> roleIds);
	
	/**
	 * 保存角色菜单关系数据
	 * @param roleId
	 * @param menuIds
	 * @return
	 */
	int insertObjects(Integer roleId,Integer[]menuIds);
	/**
	 *	基于菜单id删除角色菜单关系表数据
	 * @param id
	 * @return
	 */
	@Delete("delete from sys_role_menus where role_id=#{id}")
	int deleteObjectByRoleId(Integer id);
	 /**
	  *	基于菜单id删除角色菜单关系表数据
	  * @param id
	  * @return
	  */
	 @Delete("delete from sys_role_menus where menu_id=#{id}")
	 int deleteObjectByMenuId(Integer id);
}
