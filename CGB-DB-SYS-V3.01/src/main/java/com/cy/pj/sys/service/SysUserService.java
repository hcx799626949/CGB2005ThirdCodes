package com.cy.pj.sys.service;

import java.util.Map;

import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.pojo.SysUserDept;

public interface SysUserService {
	/**
	 *	 修改登录用户密码
	 * @param sourcePassword
	 * @param newPassword
	 * @param cfgPassword
	 * @return
	 */
	int updatePassword(String sourcePassword,String newPassword,String cfgPassword);
	
	 /**
	  * 	基于用户id查询用户以及用户对应部门,用户对应的角色信息,并
	  * 	最终将查询结果存储到map集合.
	  * @param id
	  * @return
	  */
	 Map<String,Object> findById(Integer id);
	 
	 /**
	  * 	更新用户以及用户对应的角色关系数据
	  * @param entity
	  * @param roleIds
	  * @return
	  */
	 int updateObject(SysUser entity,Integer[]roleIds);
	  /**
	   * 	保存用户以及用户对应的角色关系数据
	   * @param entity
	   * @param roleIds
	   * @return
	   */
	  int saveObject(SysUser entity,Integer[]roleIds);
	
	  int validById(Integer id,Integer valid);

	  PageObject<SysUserDept> findPageObjects(String username,
			  Integer pageCurrent);
}
