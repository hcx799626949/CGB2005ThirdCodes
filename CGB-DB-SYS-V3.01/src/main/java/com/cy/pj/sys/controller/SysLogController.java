package com.cy.pj.sys.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cy.pj.common.pojo.JsonResult;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysLog;
import com.cy.pj.sys.service.SysLogService;

@Controller
@RequestMapping("/log/")
public class SysLogController {

	 @Autowired
	 private SysLogService sysLogService;
	 
	 
	 
	 @RequestMapping("doDeleteObjects")
	 @ResponseBody
	 public JsonResult doDeleteObjects(Integer... ids) {
		 System.out.println("ids="+Arrays.toString(ids));
		 sysLogService.deleteObjects(ids);
		 //调用业务方法执行删除操作
		 return new JsonResult("delete ok");
	 }
	 
	 @RequestMapping("doFindPageObjects")
	 @ResponseBody
	 public JsonResult doFindPageObjects(Long pageCurrent,String username){
//		 try {
//		   JsonResult r=new JsonResult();
//		   r.setData(sysLogService.findPageObjects(pageCurrent, username));
//		   return r;
		   return new JsonResult(sysLogService.findPageObjects(pageCurrent, username));
//		 }catch(Throwable e) {
//		   return new JsonResult(e);
//		 }
	 }

//    在controller类的内部也可以定义异常处理方法,只是不常用而已	 
//	  @ExceptionHandler(RuntimeException.class)
//	  @ResponseBody
//	  public JsonResult doHandleRuntimeException(RuntimeException e) {
//		   System.out.println("SysLogController.doHandleRuntimeException");
//		   e.printStackTrace();//在控制台输出异常信息
//		   return new JsonResult(e);
//	  }
//	
}




