package com.cy.pj.common.pojo;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 	基于此对象实现控制层数据的封装,为业务层的返回结果
 * 	添加状态信息.
 * @author Administrator
 */
@Data
@NoArgsConstructor
public class JsonResult implements Serializable{//注意:此类中所有属性名必须按我定义名字去写.
	 private static final long serialVersionUID = 5110901796917551720L;
     /**状态码:用于标识服务端响应到客户端数据的状态*/
	 private Integer state=1;//1 正确,0 错误
	 /**用于记录状态码对象的状态信息*/
	 private String message="ok";
	 /**这个属性用于存储业务层返回给控制层的数据或者是控制层自己封装的数据*/
	 private Object data;//为什么要用object类型?(业务层返回给控制层数据不知道什么类型)
	 
	 /**当保存,删除,更新更新以后,给出一个消息,可以调用此构造函数封装消息,例如 update Ok*/
	 public JsonResult(String message){
		 this.message=message;
	 }
	 
	 /**假如希望封装业务层查询到数据,可以使用此构造函数*/
	 public JsonResult(Object data){
		 this.data=data;
	 }
	 
	 /**假如希望封装控制层出现的异常信息,可以使用此构造函数*/
	 public JsonResult(Throwable e){
		 this.state=0;
		 this.message=e.getMessage();
	 }
	
}







