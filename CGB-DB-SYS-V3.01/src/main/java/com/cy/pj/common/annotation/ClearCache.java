package com.cy.pj.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * @Retention 注解用于定义注解何时有效
 * @Target 注解用于定义注解可以描述的对象
 * @Documented 将注解中的文档注释在提取也要生成API文档
 * @author Administrator
 */
@Retention(RetentionPolicy.RUNTIME)//@Retention注解用于定义注解何时有效
@Target(ElementType.METHOD) //@Target注解用于定义注解可以描述的对象
@Documented //将注解中的文档注释在提取也要生成API文档
public @interface ClearCache {

}
