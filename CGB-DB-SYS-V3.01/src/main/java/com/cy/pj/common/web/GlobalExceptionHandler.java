package com.cy.pj.common.web;

import org.apache.shiro.ShiroException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cy.pj.common.pojo.JsonResult;
/**
 * 	@ControllerAdvice 注解描述的类为spring web (MVC)
 *	控制层的一个全局异常处理对象,当控制层出现异常以后,首先会检测控制层类中是否有异常处理方法,
 *	假如没有或者不能处理当前异常,则会查找是否有@ControllerAdvice注解描述的类,然后检测全局异常处理类中是否
 *	有合适的异常处理方法.假如如有则直接调用方法处理异常.
 *
 *	思考:为什么要做全局异常处理?(提取共性)
 *  	
 */
//@ControllerAdvice
//@ResponseBody
@RestControllerAdvice  //@ControllerAdvice+@ResponseBody
public class GlobalExceptionHandler {
	    /**
	     * 	处理shiro框架异常
	     * @param e
	     * @return
	     */
	    @ExceptionHandler(ShiroException.class) 
	    //@ResponseBody
		public JsonResult doHandleShiroException(
				ShiroException e) {
			JsonResult r=new JsonResult();
			r.setState(0);
			if(e instanceof UnknownAccountException) {
				r.setMessage("账户不存在");
			}else if(e instanceof LockedAccountException) {
				r.setMessage("账户已被禁用");
			}else if(e instanceof IncorrectCredentialsException) {
				r.setMessage("密码不正确");
			}else if(e instanceof AuthorizationException) {
				r.setMessage("没有此操作权限");
			}else {
				r.setMessage("系统维护中");
			}
			e.printStackTrace();
			return r;
		}

       /**
        * 	  @ExceptionHandler 注解描述的方法为一个异常处理方法,注解中定义的
        * 	异常类型,为当前方法的可以处理的异常处理类型(当然也可以处理异常类型的子类类
        * 	型).在异常处理方法中通常会定义一个异常参数,来接收异常对象.
        * @return
        */
	   @ExceptionHandler(RuntimeException.class)
	   //@ResponseBody
	   public JsonResult doHandleRuntimeException(RuntimeException e) {
		   System.out.println("GlobalExceptionHandler.doHandleRuntimeException");
		   e.printStackTrace();//在控制台输出异常信息
		   return new JsonResult(e);
	   }
	   //后续还会在此类中定义异常处理方法.
}







