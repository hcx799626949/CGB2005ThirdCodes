package com.cy.pj.goods.service;
import java.util.List;
import com.cy.pj.goods.pojo.Goods;

public interface GoodsService {
	
	int updateObject(Goods entity);
	
	Goods findById(Integer id);
	
	/**
	   * 保存商品信息
	 * @param entity
	 * @return
	 */
	int saveObject(Goods entity);
	/**
	  * 获取所有商品信息
	 * @return
	 */
	List<Goods> findGoods();

	 /**
	    * 基于id执行删除操作
	  * @param id 商品id
	  * @return 删除的行数
	  */
	 int deleteById(Integer id);
}
