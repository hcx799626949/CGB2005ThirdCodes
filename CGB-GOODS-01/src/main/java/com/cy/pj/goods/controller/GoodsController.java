package com.cy.pj.goods.controller;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;

/**此对象为spring mvc架构中的后端处理器对象*/
@Controller
@RequestMapping("/goods/")
public class GoodsController {//"/goods/doGoodsUI"~GoodsController.doGoodsUI
	
	@Autowired //先找类型,再找名字
	//@Resource //先找名字,再找类型
	private GoodsService goodsService;
	
	//==================update=================
	
	@RequestMapping("doFindById/{id}")
	public String doFindById(@PathVariable Integer id,Model model) {
		Goods goods=goodsService.findById(id);
		model.addAttribute("goods", goods);
		return "goods-update";//view name
	}
	
	@RequestMapping("doUpdateGoods")
	public String doUpdateGoods(Goods entity) {
		goodsService.updateObject(entity);
		return "redirect:/goods/doGoodsUI";
	}
	
	//=====================add=====================
	@RequestMapping("doGoodsAddUI")
    public String doGoodsAddUI() {
    	return "goods-add";
    }
	@RequestMapping("doSaveGoods")
	public String doSaveGoods(Goods entity) {
		goodsService.saveObject(entity);
		return "redirect:/goods/doGoodsUI";
	}
	//=====================delete=========================
	//restful 风格url的定义(restful为一种软件架构编码风格,定义了一种url的格式)
	//restful 风格的url语法:/a/b/{c}/{d},在这样的语法结构中{}为一个变量表达式
	@RequestMapping("doDeleteById/{id}")//"/goods/doDeleteById/{id}
	//假如我们希望在方法参数中获取rest url中变量表达式的值,可以使用@PathVariable注解对参数进行描述
	public String doDeleteById(@PathVariable Integer id,Model model) {
		System.out.println("id="+id);
		goodsService.deleteById(id);
		List<Goods> list=goodsService.findGoods();
		model.addAttribute("list", list);
		return "goods";
		//return "forward:/goods/doGoodsUI";//转发也要基于url找资源,也要通过反射调方法
		//return "redirect:/goods/doGoodsUI";//这种写法性能比较差(客户端与服务端需要重新建立连接)
	}
	
	/**
     * 	此方法为GoodsController中处理页面请求的一个方法,
     * 	这方法的返回值现在是一个模板页面.
     * 	思考:
     * 	这个方法由谁调用?(前端控制器DispatcherServlet调用)
     * 	这个方法的返回值传递谁?(谁调用此方法这个方法就的返回值就给谁.)
     * 	这个方法的返回值后续要做怎样的处理?(DispatcherServlet调用视图解析进行解析,
     * 	添加对应的前缀后缀,构成/templates/pages/goods.html)
     */
	@RequestMapping("doGoodsUI")
	public String doGoodsUI(Model model) {//model对象可以将数据存储到请求作用域
		List<Goods> list=goodsService.findGoods();
		model.addAttribute("list", list);
		return "goods";
	}
}
