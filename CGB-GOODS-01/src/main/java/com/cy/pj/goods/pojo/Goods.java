package com.cy.pj.goods.pojo;

import java.util.Date;

import org.springframework.stereotype.Component;

/**
 *    基于这个类型的对象,存储从商品表中查询到的产品信息
 *    要求:
 * 1)一行记录映射为内存中的一个Goods类型的对象
 * 2)Goods类中的属性要与表中的字段有对应的映射关系(
 *        例如名字和类型尽量下共同)
 */
public class Goods {
	private Long id;
	private String name;
	private String remark;
	private Date createdTime;//java.util.Date
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	@Override
	public String toString() {
		return "Goods [id=" + id + ", name=" + name + ", remark=" + remark + ", createdTime=" + createdTime + "]";
	}

}
