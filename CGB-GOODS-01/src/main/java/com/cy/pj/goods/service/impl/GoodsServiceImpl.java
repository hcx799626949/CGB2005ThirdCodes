package com.cy.pj.goods.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
/***
 *    在业务对象中重点对于数据逻辑进行业务拓展,例如
 * 1)日志的记录
 * 2)事务的控制
 * 3)权限控制
 * 4)异步操作
 * 5)缓存操作
 * 6)................
 */
@Service
public class GoodsServiceImpl implements GoodsService{
    /**获取一个日志对象*/
	private static final Logger log=
			 LoggerFactory.getLogger(GoodsServiceImpl.class);
	@Autowired
	private GoodsDao goodsDao;
	
	@Override
	public Goods findById(Integer id) {
		//其它业务暂时不写,...
		return goodsDao.findById(id);
	}
	
	
	@Override
	public int updateObject(Goods entity) {
		int rows=goodsDao.updateObject(entity);
		return rows;
	}
	@Override
	public int saveObject(Goods entity) {
		int rows=goodsDao.insertObject(entity);
		return rows;
	}
	
	@Override
	public List<Goods> findGoods() {
		long t1=System.currentTimeMillis();
		List<Goods> list= goodsDao.findGoods();
		long t2=System.currentTimeMillis();
		log.info("execute time:{}",(t2-t1));
		return list;
	}
	
	@Override
	public int deleteById(Integer id) {
		long t1=System.currentTimeMillis();
	    int rows=goodsDao.deleteById(id);
	    long t2=System.currentTimeMillis();
	    //System.out.println("execute time:"+(t2-t1));
	    log.info("execute time:{}",(t2-t1));//可以输出到控制台,写可以写入到文件
	    return rows;
	}
}






