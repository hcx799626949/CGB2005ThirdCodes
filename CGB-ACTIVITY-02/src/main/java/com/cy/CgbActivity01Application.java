package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CgbActivity01Application {

	public static void main(String[] args) {
		//System.out.println("启动");
		SpringApplication.run(CgbActivity01Application.class, args);
	}

}
