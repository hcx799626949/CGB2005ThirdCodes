package com.cy.pj.activity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cy.pj.activity.pojo.Activity;
import com.cy.pj.activity.service.ActivityService;

@Controller
public class ActivityController {
	 @Autowired
	 private ActivityService activityService;
	 
	 @RequestMapping("/activity/doDeleteObject/{id}")
	 public String doDeleteObject(@PathVariable Long id) {
		 System.out.println("delete.id="+id);
		 activityService.deleteObject(id);
		 return "redirect:/activity/doFindActivitys";
	 }
	 
	 @RequestMapping("/activity/doSaveActivity")
	 public String doSaveActivity(Activity activity) {
		 System.out.println("activity="+activity);//检查客户端提交的数据
		 activityService.saveActivity(activity);
		 return "redirect:/activity/doFindActivitys";
	 }
	 
	 /**查询所有活动信息
	  *      页面和数据一起返回,这种写法的问题:
	  * 1)当业务数据加载非常慢,页面也响应的速度也会慢.
	  * 2)现在在这种大前端架构下,各种端百花齐放(浏览器,手机端,手表端,....),假如有些端只要数据,不需要你的页面.
	      *        你也返回一个页面这时没有的
	  * */
//	 @RequestMapping("/activity/doFindActivitys")
//	 public String doFindActivitys(Model model) {
//		 List<Activity> list=activityService.findActivitys();
//		 model.addAttribute("list", list);
//		 return "activity";
//	 }
	 
	 @RequestMapping("/activity/doFindActivitys")
	 @ResponseBody
	 //当我们方法返回值为一个或多pojo对象时(也可是map)
	 //spring mvc还会检测方法上是否有@ResponseBody注解,
	 //假如有这个注解,spring mvc还会将返回的对象转换为json格式字符串.
	 public List<Activity> doFindActivitys(){
		 return activityService.findActivitys();
	 }
	 
	 @RequestMapping("/activity/doActivityUI")
	 public String doActivityUI() {
		 return "activity";
	 }
	 
}






