package com.cy.pj.activity.service;
import java.util.List;
import com.cy.pj.activity.pojo.Activity;
//引入包中的类:ctrl+shift+o

public interface ActivityService {
	
	int deleteObject(Long id);
	int saveActivity(Activity entity);
	List<Activity> findActivitys();
}
