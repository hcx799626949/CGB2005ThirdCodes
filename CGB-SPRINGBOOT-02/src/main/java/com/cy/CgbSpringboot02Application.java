package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import com.cy.pj.common.pool.ObjectPool;

@SpringBootApplication
public class CgbSpringboot02Application {
	
	/**
	 * @Bean 注解通常会应用在一些配置类(由@Configuration注解描述)中,用于描述
	  *    具备返回值的方法,然后系统底层会通过反射调用其方法,获取对象基于作用域将对象
	  *    进行存储或应用。
	 * @Bean 应用场景：第三方资源的整合及配置。
	 * @return
	 */
	@Scope("singleton")
	@Lazy(true)
	@Bean("aaa") //Spring容器中由@Bean描述的方法的返回值,对应的名字默认为方法名
	public ObjectPool objectPool() {
		System.out.println("==objectPool()===");
		return new ObjectPool();
	}

	public static void main(String[] args) {
		SpringApplication.run(CgbSpringboot02Application.class, args);
	}

}
