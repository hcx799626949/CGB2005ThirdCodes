package com.cy.pj.common.pool;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
  *  假设此对象为一个对象池
 * 1)此对象要交给spring管理(SpringBoot工程下)
 * 1.1)类的位置(必须放在启动类所在包以及子包)
 * 1.2)类的描述(使用@Component注解)
 * 2)需要此对象的地方可从spring的bean池中获取
  *  思考:
 * 1)如何判定一个对象在何时创建? (日志,断点)
 * 2)Spring容器中的Bean对象默认在启动时创建,但长时间又不使用这个对象,
 *    你觉得先把对象创建出来好吗?(不好,占内存资源,由其是一些大对象)
 * 3)Spring容器中的Bean对象假如不希望在容器启动时进行创建,该如何设计呢?(延迟加载)
 * 
 * @Lazy注解: 描述Spring管理的bean对象时,可以让bean延迟构建和初始化.
  *   延迟加载(懒加载,按需加载)应用场景?
  *   大对象(例如一些池对象),稀少用(例如容器初始化创建对象也用不到).
   
   @Scope 描述类时用于指定类实例的作用域,常用取值:
   1)singleton :默认为单例作用域-类的实例在一个JVM内存中其实例只有一份,此作用域
      通常还会配置@Lazy一起使用,一般可重用对象可设计为单例,对于这样的对象spring
      框架不仅仅负责其创建,还会存储到bean池.
   2)prototype (多例作用域-此作用域对象何时需要何时创建,spring框架不负责销毁)
       一般不可重用对象,或者说只使用一次就不再用此对象了,这样的对象就可设计为多例
      作用域.
 */
//@Scope //spring容器中的bean没有使用@Scope注解描述时默认就是单例.
//@Scope("prototype")
//@Lazy
//@Lazy //默认value属性值为true,表示此对象要延迟构建及初始化.
//@Lazy(false)//这里假如要写false,跟没有写@Lazy注解是一样的,表示不支持对象延迟构建及初始化.
//@Component //此注解描述bean表示这个bean对象会由spring创建,其名字默认为类名,首字母小写.
public class ObjectPool {//bean的名字objectPool
   public int size;
   
   public ObjectPool() {//构造方法执行了,说明对象创建了
	   System.out.println("ObjectPool()");
   }
   /**
    * @PostConstruct 此注解描述的方法为生命周期初始化方法
         * 在这样的方法中一般会为创建好的对象再此进行一些初始化.
    */
   @PostConstruct
   public void init() {
	   System.out.println("init()");
   }
   /**
    *@PreDestroy 此注解描述的方法为生命周期销毁方法,此方法会在对象销毁之前执行.
         * 在这样的方法中可以实现一些,资源销毁操作.
    */
   @PreDestroy
   public void destory() {//将对象从bean池中移除之前会执行此方法
	  System.out.println("destory()");
   } 
}

