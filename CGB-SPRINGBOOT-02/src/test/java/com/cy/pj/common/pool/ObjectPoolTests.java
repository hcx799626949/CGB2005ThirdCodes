package com.cy.pj.common.pool;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * SpringBoot 2.x 工程下的测试类有什么特点?
 * 1)包结构(启动类所在包或子包)
 * 2)注解描述(@SpringBootTest)
 * 
 * SpringBoot 2.x 工程下的单元测试方法有什么特点?
 * 1)单元测试方法使用@Test注解(org.junit.jupiter.api.Test)进行描述.
 * 2)单元测试方法不能使用private访问修饰符
 * 3)单元测试方法返回值只能是void类型
 * 4)单元测试方法不允许写方法参数
 * 5)单元测试方法允许抛出异常.
 * 
 * SpringBoot单元测试类中如何为属性进行值的注入?
 * 1)单元测试类使用了@SpringBootTest注解描述
 * 2)测试类中的属性使用Spring框架中的指定注解进行描述(例如@Autowired)
 * 
 */
@SpringBootTest
public class ObjectPoolTests {
     @Autowired
	 private ObjectPool objectPool1;
     
     @Autowired
     private ObjectPool objectPool2;
	 
	 @Test
	 void testObjectPool() {
		 System.out.println("objectPool1="+objectPool1);
		 System.out.println(objectPool1==objectPool2);//比较对象地址
		 objectPool1.size=10;
		 System.out.println(objectPool2.size);
	 }
}


