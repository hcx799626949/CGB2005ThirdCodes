package com.cy.pj.common.pool;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
@SpringBootTest
public class BeanFactoryTests {
	//spring框架中的顶层工厂接口
	@Autowired
	private BeanFactory beanFactory;
	//问题:请求spring中同一个类型,但不同名字的配置,是否会存在多个这个类型的实例?
	@Test
	void testBean() {
		//通过工厂基于bean的名字,从bean池中获取对象
		ObjectPool op01=beanFactory.getBean("objectPool", ObjectPool.class);
		System.out.println("op01="+op01);
		ObjectPool op02=beanFactory.getBean("aaa", ObjectPool.class);
		System.out.println("op02="+op02);
		System.out.println(op01==op02);//false
	}
}
