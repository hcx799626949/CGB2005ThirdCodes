package com.cy.pj.common.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(2)
@Aspect
@Component
public class SysTimeAspect {

	 @Pointcut("bean(sysUserServiceImpl)")
	 public void doTime() {
		 //不写具体内容
	 }
	 @Before("doTime()")
	 public void doBefore() {
		 System.out.println("@Before");
	 }
	 @After("doTime()")
	 public void doAfter() {
		 System.out.println("@After");
	 }
	 @AfterReturning("doTime()")
	 public void doAfterReturning() {
		 System.out.println("@AfterReturning");
	 }
	 @AfterThrowing("doTime()")
	 public void doAfterThrowing() {
		 System.out.println("@AfterThrowing");
	 }
	 @Around("doTime()")
	 public Object doAround(ProceedingJoinPoint jp) throws Throwable {
		 try {
		   System.out.println("@Around.before");
		   Object result=jp.proceed();//执行目标方法
		   System.out.println("@Around.after");
		   return result;
		 }catch(Throwable e) {
			//e.printStackTrace();
		   System.out.println("@Around.throwing");
		   throw e;
		 }
	 }
}
