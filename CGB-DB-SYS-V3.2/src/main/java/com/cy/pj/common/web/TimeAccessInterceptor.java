package com.cy.pj.common.web;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import com.cy.pj.common.exception.ServiceException;
/**
 * 	Spring Web模块提供了一个拦截器类型(HandlerInterceptor),通过这样的拦截器可以在Spring Web模块后端的Controller
 *	 之前后之后做一些预处理.
 */
public class TimeAccessInterceptor implements HandlerInterceptor {

	/**此方法会在你的目标Controller执行之前执行*/
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("==preHandler==");
		LocalDateTime now=LocalDateTime.now();//JDK8(获取当前日期和时间)
		int current=now.getHour();
		System.out.println("start="+current);
	    if(current<9||current>19)
	    	throw new ServiceException("不在访问时间:9~14");//return false
		return true;//true表示请求放行,false表示请求到此结束.
	}
}







