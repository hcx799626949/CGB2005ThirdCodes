package com.cy.pj.common.exception;
/**
 * 	用于封装业务层异常信息的一个异常对象类型
 */
public class ServiceException extends RuntimeException {
	private static final long serialVersionUID = -5598865415547474216L;
	public ServiceException() {
		super();
	}
	public ServiceException(String message) {
		super(message);
	}
	public ServiceException(Throwable cause) {
		super(cause);
	}
}
