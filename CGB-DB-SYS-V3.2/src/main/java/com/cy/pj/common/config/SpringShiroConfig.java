package com.cy.pj.common.config;

import java.util.LinkedHashMap;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.cy.pj.sys.service.realm.ShiroUserRealm;

//配置参考:http://shiro.apache.org/spring-boot.html

@Configuration //此注解描述的类为spring容器中的一个配置类
public class SpringShiroConfig {
    /**配置realm对象*/
	@Bean
	public Realm realm() {
		return new ShiroUserRealm();
	}
	/**配置过滤规则*/
	@Bean
	public ShiroFilterChainDefinition shiroFilterChainDefinition() {
		DefaultShiroFilterChainDefinition chainDefinition = new DefaultShiroFilterChainDefinition();
		LinkedHashMap<String,String> map= new LinkedHashMap<>();
		//静态资源允许匿名访问:"anon"
		map.put("/bower_components/**","anon");
		map.put("/build/**","anon");
		map.put("/dist/**","anon");
		map.put("/plugins/**","anon");
		map.put("/user/doLogin","anon");
		map.put("/doLogout","logout");//logout对应着shiro框架中退出过滤器
		//除了匿名访问的资源,其它都要认证("authc")后访问
		map.put("/**","user");//假如需要实现记住我功能,这里的过滤器标识使用user
		chainDefinition.addPathDefinitions(map);
		return chainDefinition;
	}
	/***
	 * 	配置缓存对象,用于缓存用户的权限信息
	 * @return
	 */
	@Bean
	protected CacheManager shiroCacheManager() {
	    return new MemoryConstrainedCacheManager();
	}

}





