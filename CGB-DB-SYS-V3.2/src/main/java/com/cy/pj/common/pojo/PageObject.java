package com.cy.pj.common.pojo;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * PageObject 为业务层封装分页相关数据的一个BO对象.
 * @param <T> 参数化的类型(泛型)
 * 	类泛型(这里的泛型用于约束类中属性,方法参数,方法的返回值类型)
 */
@Data
@AllArgsConstructor
public class PageObject<T> implements Serializable{//类名<泛型>:
	private static final long serialVersionUID = -7452188623635031202L;
	/**记录总数*/
	private Long rowCount;
	/**当前页记录*/
	private List<T> records;
	/**总页数*/
	private Long pageCount;
	/**页面大小*/
	private Integer pageSize;
	/**当前页面值*/
	private Long pageCurrent;
	
	public PageObject() {}

	public PageObject(Long rowCount, List<T> records, Integer pageSize, Long pageCurrent) {
		super();
		this.rowCount = rowCount;
		this.records = records;
		this.pageSize = pageSize;
		this.pageCurrent = pageCurrent;
		//计算总页数
		//方法1:
//		this.pageCount=rowCount/pageSize;
//		if(rowCount%pageSize!=0) {
//			pageCount++;
//		}
		//方法2:
		this.pageCount=(rowCount-1)/pageSize+1;
	}
    
	
}




