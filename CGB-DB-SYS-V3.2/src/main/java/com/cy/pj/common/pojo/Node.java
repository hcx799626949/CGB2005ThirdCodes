package com.cy.pj.common.pojo;

import java.io.Serializable;

import lombok.Data;

/**
 * 	定义树节点(例如zTree)对象类型
 */

@Data
public class Node implements Serializable{
	private static final long serialVersionUID = 6904860186603541043L;
	private Integer id;
	private String name;
	private String parentId;
}
