package com.cy.pj.sys.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.dao.SysMenuDao;
import com.cy.pj.sys.dao.SysRoleMenuDao;
import com.cy.pj.sys.dao.SysUserRoleDao;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.pojo.SysUserMenu;
import com.cy.pj.sys.service.SysMenuService;

@Service
public class SysMenuServiceImpl implements SysMenuService {

	@Autowired
	private SysMenuDao sysMenuDao;
	
	@Autowired
	private SysRoleMenuDao sysRoleMenuDao;
	
	@Autowired
	private SysUserRoleDao sysUserRoleDao;
	
	@Override
	public List<SysUserMenu> findUserMenusByUserId(Integer id) {
		//1.基于用户id查找用户对应的角色id
		List<Integer> roleIds=sysUserRoleDao.findRoleIdsByUserId(id);
		//2.基于角色id查找用户对应的菜单id
		List<Integer> menuIds=sysRoleMenuDao.findMenuIdsByRoleIds(roleIds);
		//3.基于菜单id查找用户对应的菜单信息
		return sysMenuDao.findMenusByIds(menuIds);
	}
	
	@Override
	public int saveObject(SysMenu entity) {
		//1.参数校验
		//..........
		//2.保存菜单信息
		int rows=sysMenuDao.insertObject(entity);
		return rows;
	}
	
	@Override
	public int updateObject(SysMenu entity) {
		//1.参数校验
		//..........
		//2.保存菜单信息
		int rows=sysMenuDao.updateObject(entity);
		if(rows==0)
			throw new ServiceException("记录可能已经不存在");
		return rows;
	}
	
	@Override
	public List<Node> findZtreeMenuNodes() {
		// TODO Auto-generated method stub
		return sysMenuDao.findZtreeMenuNodes();
	}
	/**@CacheEvict 注解描述的方法也是一个缓存切入点方法,这个方法会在目标方法执行之前或之后执行缓存清除动作
	 * 1) value 属性用于指定缓存对象的名称
	 * 2) allEntries 用于指定是否清楚缓存所有数据
	 * 3) beforeInvocation用于指定清楚缓存的动作在目标方法执行之前还是之后执行.这里的false表示之后
	 * */
	@CacheEvict(value = "menuCache",allEntries = true,beforeInvocation = false )
	@Override
	public int deleteObject(Integer id) {
	    //1.参数校验
		if(id==null||id<1)throw new IllegalArgumentException("id值无效");
		//2.判定是否有子菜单
		int childCount=sysMenuDao.getChildCount(id);
		if(childCount>0)throw new ServiceException("请先删除子菜单");
		//3.执行菜单角色关系数据的删除
		//sysRoleMenuDao.deleteObjectByMenuId(id);
		sysRoleMenuDao.deleteObjects("menu_id", id);
		//4.删除自身信息
		int rows=sysMenuDao.deleteObject(id);
		//5.验证结果并返回
		if(rows==0)throw new ServiceException("记录可能已经不存在");
		return rows;
	}
	/**@Cacheable 注解描述的方法为一个缓存切入点方法,此方法在执行之前首先会从缓存取数据,缓存没有数据,则从数据库查询*/
	@Cacheable(value = "menuCache")
	@Override
	public List<Map<String, Object>> findObjects() {
		System.out.println("SysMenuServiceImpl.findObjects()");
		return sysMenuDao.findObjects();
	}

}
