package com.cy.pj.sys.service;

import java.util.List;
import java.util.Map;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.pojo.SysUserMenu;

public interface SysMenuService {
	
	  /**
	   *	  查找用户对应的菜单
	   * @param id
	   * @return
	   */
	  List<SysUserMenu> findUserMenusByUserId(Integer id);
	
	  int updateObject(SysMenu entity);
	  int saveObject(SysMenu entity);
	
	  List<Node> findZtreeMenuNodes();
	  /**
	   * 	基于菜单id删除菜单角色关系数据,菜单自身信息.
	   * @param id
	   * @return
	   */
	  int deleteObject(Integer id);

	  List<Map<String,Object>> findObjects();
}
