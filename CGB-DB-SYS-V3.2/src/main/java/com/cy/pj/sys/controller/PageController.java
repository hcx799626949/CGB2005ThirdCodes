package com.cy.pj.sys.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cy.pj.common.util.ShiroUtils;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.pojo.SysUserMenu;
import com.cy.pj.sys.service.SysMenuService;
/**
   * 计划在这个controller中要处理所有页面请求
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/")
public class PageController {
	
	  @Autowired
	  private SysMenuService sysMenuService;
	
	  @RequestMapping("doLoginUI")
	  public String doLoginUI() {
		  return "login";
	  }
      //返回项目的首页页面
	  @RequestMapping("doIndexUI")
	  public String doIndexUI(Model model) {
		  //获取登录用户信息(登录时在real中封装用户信息传入的principal,在这里拿到的Principal就是什么)
		  SysUser user=ShiroUtils.getUser();
		  //将登录用户信息存储到model,然后再页面上基于thymeleaf获取model数据并呈现.
		  model.addAttribute("username", user.getUsername());
		  //查询用户拥有方法权限的菜单信息
		  List<SysUserMenu> userMenus=sysMenuService.findUserMenusByUserId(user.getId());
		  System.out.println("userMenus="+userMenus);
		  model.addAttribute("userMenus", userMenus);
		  return "starter";
	  }
	  //返回分页页面元素
	  @RequestMapping("doPageUI")
	  public String doPageUI() {
		  return "common/page";
	  }
	  //返回日志列表页面
//	  @RequestMapping("log/log_list")
//	  public String doLogUI() {
//		  return "sys/log_list";
//	  }
	  //返回菜单列表页面
//	  @RequestMapping("menu/menu_list")
//	  public String doMenuUI() {
//		  System.out.println("==doMenuUI==");
//		  return "sys/menu_list";
//	  }
	  
	  //rest风格(软件架构编码风格,这个风格中url的定义允许使用{}作为变量)的URL
	  @RequestMapping("{module}/{moduleUI}")
	  public String doModuleUI(@PathVariable String moduleUI) {
		 // System.out.println("==doModuleUI==");
		  return "sys/"+moduleUI;
	  }
	  //rest 风格的url优势:
	  //1)可以更通用
	  //2)可以在异构平台下进行兼容
}










