package com.cy.pj.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
/**
 * 	基于此DAO操作用户和角色关系表
 * @author Administrator
 */
@Mapper
public interface SysUserRoleDao {
	
	 @Select("select role_id from sys_user_roles where user_id=#{userId}")
	 List<Integer> findRoleIdsByUserId(Integer userId);

	 /**保存用户和角色关系数据*/
	 int insertObjects(Integer userId,Integer[]roleIds);
	 
	 /**基于用户id删除用户和角色关系数据*/
	 @Delete("delete from sys_user_roles where user_id=#{userId} ")
	 int deleteObjectsByUserId(Integer userId);
	 
	 
	 
	 
}
