package com.cy.pj.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.pojo.SysUserDept;

@Mapper
public interface SysUserDao {
	 /**
	  *	 修改密码
	  * @param username
	  * @param newPassword
	  * @param newSalt
	  * @return
	  */
	 @Update("update sys_users set password=#{newPassword},salt=#{newSalt},modifiedTime=now(),modifiedUser=#{username} where username=#{username}")
	 int updatePassword(String username,String newPassword,String newSalt);
	 
	 @Select("select * from sys_users where username=#{username}")
	 SysUser findUserByUserName(String username);
	
	 int updateObject(SysUser entity);
	  /**
	   * 	基于用户id查询用户以及用户对应的部门信息
	   * @param id
	   * @return
	   */
	  SysUserDept findById(Integer id);
	   /**
	    *	 保存用户自身信息
	    * @param entity
	    * @return
	    */
	   int insertObject(SysUser entity);
	
	   @Update("update sys_users set valid=#{valid},modifiedTime=now(),modifiedUser=#{modifiedUser} where id=#{id}")
	   int validById(Integer id,Integer valid,String modifiedUser);

//	   int getRowCount(String username);
//	   List<SysUserDept> findPageObjects(String username,Integer startIndex,Integer pageSize);

	   //使用了PageHelper以后,只需要写如下方法即可
	   List<SysUserDept> findPageObjects(String username);
}
