package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysLog;

/**
 * 	日志模块的业务接口
 * @author Administrator
 */
public interface SysLogService {
	
	 void saveObject(SysLog entity);
	  /**
	        * 基于多个id删除日志记录
	   * @param ids
	   * @return
	   */
	  int deleteObjects(Integer... ids);

	  /**
	   * 	基于条件查询当前页的用户行为日志信息
	   * @param pageCurrent 当前页的页码值(来自页面端用户的输入)
	   * @param username 用户名(来自于页面的输入)
	   * @return 查询到的分页信息
	   */
	  PageObject<SysLog> findPageObjects(Long pageCurrent,String username);
}
