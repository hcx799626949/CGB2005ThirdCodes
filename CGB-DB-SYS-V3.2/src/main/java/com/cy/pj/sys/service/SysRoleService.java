package com.cy.pj.sys.service;

import java.util.List;

import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.pojo.SysRoleMenu;


public interface SysRoleService {
	 List<CheckBox> findRoles();
	 /**
	  * 	基于角色id查询角色以及角色对应的菜单id
	  * @param id
	  * @return
	  */
	 SysRoleMenu findById(Integer id);
	 /**
	  * 	更新角色自身信息以及角色和菜单关系数据
	  * @param entity
	  * @param menuIds
	  * @return
	  */
	  int updateObject(SysRole entity,Integer[]menuIds);
	  /**
	   * 	保存角色自身信息以及角色和菜单关系数据
	   * @param entity
	   * @param menuIds
	   * @return
	   */
	  int saveObject(SysRole entity,Integer[]menuIds);
	  /**
	   * 	查询角色自身信息以及分页信息
	   * @param name
	   * @param pageCurrent
	   * @return
	   */
	  PageObject<SysRole> findPageObjects(String name,Long pageCurrent);
}


