package com.cy.ajax.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/jquery/")
public class JqueryController {
	
	 @RequestMapping("doJQueryUI")
	 public String doJQueryUI() {
		return "jquery-ajax-01";
	 }

	 @RequestMapping("doAjaxGet")
	 @ResponseBody
	 public String doAjaxGet(String msg) {
		 System.out.println("msg="+msg);
		 return "JQuery Ajax Get Request Result";
	 }
	 
	 @RequestMapping("doAjaxPost")
	 @ResponseBody
	 public String doAjaxPost(String msg) {
		 System.out.println("msg="+msg);
		 return "JQuery Ajax Post Request Result";
	 }
	 
}
