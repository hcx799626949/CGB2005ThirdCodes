package com.cy.ajax.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AjaxController {
	//====================Ajax-04====================================
	
	 @RequestMapping("doAjax04UI")
	 public String doAjax04UI() {
		return "ajax-04";//03页面在02页面的基础之上做了一个封装
	 }
	  //====================Ajax-03====================================
	
	  @RequestMapping("doAjax03UI")
	  public String doAjax03UI() {
		  return "ajax-03";//03页面在02页面的基础之上做了一个封装
	  }
	
	  //====================Ajax-02====================================
	  private List<String> names=new ArrayList<String>();//假设这是存储数据的表

	  @RequestMapping("doAjax02UI")
	  public String doAjax02UI() {
		  return "ajax-02";
	  }
	  
	  /**通过此方法校验名字是否存在*/
	  @RequestMapping("doCheck")
	  @ResponseBody
	  public String doCheck(String name) {
		 if(names.contains(name))
			 return "名字"+name+"已经存在,请选择其它名字";
		 return "名字"+name+"不存在,可以注册";
	  }
	  
	  /**将客户端请求数据写入到names对应的集合*/
	  @RequestMapping("doSave")
	  @ResponseBody
	  public String doSave(String name) {
		  System.out.println("name="+name);
		  names.add(name);
		  return "save ok";
	  }

	  //===================Ajax-01=======================================
	  @RequestMapping("doAjax01UI")
	  public String doAjax01UI() {
		  return "ajax-01";
	  }
	  
	  //处理传统请求的服务端设计
	  @RequestMapping("doGet")
	  public String doGet(Model model) {
		  model.addAttribute("message", "Get request result");
		  return "ajax-01";
	  }
	
	  //处理ajax请求的服务端设计
	  @RequestMapping("doAjaxGet")
	  @ResponseBody //将方法返回值以字符串形式进行输出
	  public String doAjaxGet() throws Exception{
		  System.out.println("==doGet()==");
		  //Thread.sleep(3000);//模拟耗时操作
		  return "Ajax Get request result";
	  }
	  
}
