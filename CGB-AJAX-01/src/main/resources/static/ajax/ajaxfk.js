(function(){
  //定义构造函数
  var ajax=function(){};
  //在构造函数的原型对象上添加函数
  ajax.prototype={
       doAjaxGet:function(url,params,callback){
               	//1.创建XHR对象
     	var xhr=new XMLHttpRequest();
     	//2.设置状态监听
     	xhr.onreadystatechange=function(){
     	     if(xhr.readyState==4&&xhr.status==200){
     	    	 callback(xhr.responseText);//处理服务端响应的结果数据
     	     }
     	};
     	//3.打开链接
     	xhr.open("GET",url+"?"+params,true);
     	//4.发送请求
     	xhr.send(null);//Get请求send方法传值
       },
       
       doAjaxPost:function(url,params,callback){
        //1.创建XHR对象
     	var xhr=new XMLHttpRequest();
     	//2.设置状态监听
     	xhr.onreadystatechange=function(){
     	     if(xhr.readyState==4&&xhr.status==200){
     	    	 callback(xhr.responseText);
     	     }
     	};
     	//3.打开链接
     	//var name=document.getElementById("nameId").value;
     	var  name=document.forms[0].name.value;
     	xhr.open("POST",url,true);
     	//post请求要设置请求头(规定)
     	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     	//4.发送请求
     	xhr.send(params);//Post请求send方法传值 
       }
  }
   //通过一个全局变量来指向它我们构建的对象.
    window.Ajax=new ajax();
})()//函数自调用(基于此方式来防止js中的污染事件)