 //Ajax Post请求的封装
 function doAjaxPost(url,params,callback){
    	//1.创建XHR对象
     	var xhr=new XMLHttpRequest();
     	//2.设置状态监听
     	xhr.onreadystatechange=function(){
     	     if(xhr.readyState==4&&xhr.status==200){
     	    	 callback(xhr.responseText);
     	     }
     	};
     	//3.打开链接
     	//var name=document.getElementById("nameId").value;
     	var  name=document.forms[0].name.value;
     	xhr.open("POST",url,true);
     	//post请求要设置请求头(规定)
     	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     	//4.发送请求
     	xhr.send(params);//Post请求send方法传值 
}

//定义一个函数这个函数主要用于发送Ajax GET请求,目的封装共性,提取特性,以实现代码的可重用性
function doAjaxGet(url,params,callback){
    	//1.创建XHR对象
     	var xhr=new XMLHttpRequest();
     	//2.设置状态监听
     	xhr.onreadystatechange=function(){
     	     if(xhr.readyState==4&&xhr.status==200){
     	    	 callback(xhr.responseText);//处理服务端响应的结果数据
     	     }
     	};
     	//3.打开链接
     	xhr.open("GET",url+"?"+params,true);
     	//4.发送请求
     	xhr.send(null);//Get请求send方法传值
}