package com.cy.pj.common.util;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerScheduledTask {

	 public static void schedule(TimerTask task,Date date) {
		 Timer timer=new Timer();
		 timer.schedule(task, date);
	 }
}
