package com.cy.pj.activity.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**基于此类存储活动信息*/
//@Data
@Setter
@Getter
@ToString
@NoArgsConstructor //添加无参构造函数
@AllArgsConstructor //添加全参构造函数
public class Activity {//当此类在编译时,lombok会自动基于类中属性添加相关方法到class文件
	private Long id;
	/**活动标题*/
	private String title;
	/**活动类型*/
	private String category;
	/**活动开始时间*/
	//Spring MVC基于@DateTimeFormat指定的日期格式接收客户端提交数据,假如没有指定默认为yyyy/MM/dd
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
	private Date startTime;//java.util.Date
	/**活动结束时间*/
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
	private Date endTime;
	/**活动备注*/
	private String remark;
	/**活动状态(已启动,已结束,...)*/
	private Integer state=1;
	/**创建用户:一般是登录用户*/
	private String createdUser;
	/**创建时间:用户无需自己填写,由底层系统生成*/
	private Date createdTime;
}


