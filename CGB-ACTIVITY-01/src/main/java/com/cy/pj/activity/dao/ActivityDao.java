package com.cy.pj.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cy.pj.activity.pojo.Activity;

@Mapper
public interface ActivityDao {
	
	 @Update("update tb_activity set state=0 where id=#{id}")
	 int updateState(Long id);
	
	 @Delete("delete from tb_activity where id=#{id}")
	 int deleteObject(Long id);
	
	 @Insert("insert into tb_activity "
	 		+ "(title,category,startTime,endTime,remark,state,createdUser,createdTime)  "
	 		+ "values  "
	 		+ "(#{title},#{category},#{startTime},#{endTime},#{remark},#{state},#{createdUser},now())")
	 @Options(useGeneratedKeys = true,keyProperty = "id")
	 int insertObject(Activity activity);
     /**
                *   查询所有活动信息,一行记录映射为内存中的一个Activity对象
        1)谁负责与数据库交互?(SqlSession,再具体一点就是JDBC API)
        2)谁负责将ResultSet结果集合中的数据取出来存储到Activity?(MyBatis,再具体一点就是ResultSetHandler)
      */
     @Select("select * from tb_activity order by createdTime desc")
	 List<Activity> findActivitys();
}






