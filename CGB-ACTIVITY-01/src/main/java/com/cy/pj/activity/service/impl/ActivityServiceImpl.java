package com.cy.pj.activity.service.impl;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.pj.activity.dao.ActivityDao;
import com.cy.pj.activity.pojo.Activity;
import com.cy.pj.activity.service.ActivityService;
import com.cy.pj.common.util.TimerScheduledTask;

@Service
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private ActivityDao activityDao;
	
	@Override
	public int deleteObject(Long id) {
		//校验活动状态?(正在进行中的活动不允许进行删除)
		//.........
		//删除活动
		int rows=activityDao.deleteObject(id);
		return rows;
	}
	
	@Override
	public int saveActivity(Activity entity) {
		int rows=activityDao.insertObject(entity);
		//??????
		//开启活动倒计时(活动到了结束时间应该将其状态修改为0)
		//方案:(自己尝试)
		//1)Java 官方:
		//1.1)Timer
		//1.2)ScheduledExecutorService
		//2)借助第三方的任务调度框架(任务调度框架,quartz)
		//方案1:Timer应用
		//1.1构建Timer对象
		Timer timer=new Timer();//此对象可以负责去执行一些任务(这个对象内置一个线程和一个任务队列)
		//1.2启动线程执行任务
		timer.schedule(new TimerTask() {//TimerTask为任务
			@Override
			public void run() {//一旦调用此任务的线程获得了CPU就会执行这个任务的run方法
				activityDao.updateState(entity.getId());
				timer.cancel();//退出任务调度(后续线程也会销毁)
			}
		}, entity.getEndTime());//按指定时间执行任务.
		
//		TimerScheduledTask.schedule(new TimerTask() {
//			@Override
//			public void run() {
//				activityDao.updateState(entity.getId());
//				cancel();
//			}
//		}, entity.getEndTime());
		
		return rows;
	}
	
	@Override
	public List<Activity> findActivitys() {
		 return  activityDao.findActivitys();
	}
}
