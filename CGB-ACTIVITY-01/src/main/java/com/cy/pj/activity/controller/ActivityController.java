package com.cy.pj.activity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cy.pj.activity.pojo.Activity;
import com.cy.pj.activity.service.ActivityService;

@Controller
public class ActivityController {
	 @Autowired
	 private ActivityService activityService;
	 
	 @RequestMapping("/activity/doDeleteObject/{id}")
	 public String doDeleteObject(@PathVariable Long id) {
		 System.out.println("delete.id="+id);
		 activityService.deleteObject(id);
		 return "redirect:/activity/doFindActivitys";
	 }
	 
	 @RequestMapping("/activity/doSaveActivity")
	 public String doSaveActivity(Activity activity) {
		 System.out.println("activity="+activity);//检查客户端提交的数据
		 activityService.saveActivity(activity);
		 return "redirect:/activity/doFindActivitys";
	 }
	 
	 /**查询所有活动信息*/
	 @RequestMapping("/activity/doFindActivitys")
	 public String doFindActivitys(Model model) {
		 List<Activity> list=activityService.findActivitys();
		 model.addAttribute("list", list);
		 return "activity";
	 }
}
