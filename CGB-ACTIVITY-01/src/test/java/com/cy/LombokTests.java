package com.cy;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import com.cy.pj.activity.pojo.Activity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
public class LombokTests {
	 //当我们的类上使用了@Slf4j注解时,系统底层会自动为我们的类添加一行如下的代码
     //private static final Logger log=LoggerFactory.getLogger(LombokTests.class);
	 @Test
	 void testLombok() {
		 Activity aty=new Activity();
		 aty.setId(100L);
		 aty.setTitle("互动答疑");
		 System.out.println(aty);//toString();
		 log.info("title is {}",aty.getTitle());
	 }
}
