package com.cy;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * SpringBoot工程中的单元测试类,使用
 * @SpringBootTest 注解进行描述
 */
@SpringBootTest
class CgbSpringboot01ApplicationTests {

	@Test
	void contextLoads() {
		
	}

}
