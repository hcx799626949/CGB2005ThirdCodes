package com.cy.java.oop;

public class LogMailService extends DefaultMailService {

	@Override
	public void sendMsg(String msg) {
		System.out.println("start:"+System.currentTimeMillis());
		super.sendMsg(msg); //调用父类方法
		System.out.println("end:"+System.currentTimeMillis());
	}
}
