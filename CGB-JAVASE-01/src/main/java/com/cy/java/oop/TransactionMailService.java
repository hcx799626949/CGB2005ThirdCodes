package com.cy.java.oop;

public class TransactionMailService implements MailService {

	//has a
	private MailService mailService;
	public TransactionMailService(DefaultMailService mailService) {
		this.mailService=mailService;
	}
	@Override
	public void sendMsg(String msg) {
		System.out.println("start transactional");
		mailService.sendMsg(msg);
		System.out.println("commit transactional");
	}

}
