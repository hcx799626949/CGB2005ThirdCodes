package com.cy.java.basic;

public class TestVarParams01 {

//	static int sum(int a,int b) {
//		return a+b;
//	}
//	static int sum(int a,int b,int c) {
//		return a+b+c;
//	}
//	static int sum(int a,int b,int c,int d) {
//		return a+b+c+d;
//	}
	
	static int sum(int... array) {//可变参数,可以理解为特殊数组.
		int result=0;
		for(int i=0;i<array.length;i++) {
			result+=array[i];
		}
		return result;
	}
	
	public static void main(String[] args) {
		sum();
		sum(10,20);
		sum(10,20,30);
		sum(10,20,30,40);
	}
}
