package com.cy.java.basic;

public class TestArray01 {

	static void doMethod(int...a) {
		System.out.println(a);
	}
	public static void main(String[] args) {
		int[] a1= null;
		int[] a2= {};
		//System.out.println(a1.length);//NullPointerException
		//System.out.println(a2.length);//0
		doMethod(a1);//null
		doMethod(a2);//输出了数组的地址.
	}
}
