package com.cy.java.basic;

public class TestVar01 {//类在被加载时,所有的字节码信息都是存储在线程共享区中的方法区(例如JDK8中元数据区-Metaspace)

	static int n=100;//类变量,存储在字节码区
	int m=100;//实例变量,存储在对象内部
	public static void main(String[] args) {//参数变量,存储在方法栈(线程私有)
		int a=10;//局部变量,存储在方法栈.(线程私有)
	}//方法运行的方法信息(实际参数值,中间运算过程的值,返回值等)会存储到方法栈(以栈帧的形式进行存储)
}
