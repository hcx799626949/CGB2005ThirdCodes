package com.cy.java.jvm;
import java.util.ArrayList;
import java.util.List;

public class TestHeap01 {
	//-XX:+PrintGC
	public static void main(String[] args) {
		List<byte[]> list=new ArrayList<>();
		for(int i=0;i<10000000;i++) {
		  list.add(new byte[1024*1024]);//java.lang.OutOfMemoryError: Java heap space
		  //byte array[]=new byte[1024*1024];//下一次循环array变量的生命周期就结束
		}
	}
	//同学们:将来在写程序时对于变量的定义规则:能用局部不用参数,能有参数不用实例,能用实例不用static
}
