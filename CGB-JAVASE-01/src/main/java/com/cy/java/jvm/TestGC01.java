package com.cy.java.jvm;
import java.util.HashMap;
import java.util.Map;
class ClassA{
	/**对象在被回收前会执行此方法*/
	@Override
	protected void finalize() throws Throwable {
		System.out.println("==finalize()==");
	}
}
public class TestGC01 {
	//假设为spring中的一个bean池
	static Map<String,Object> beanPool=new HashMap<>();
	public static void main(String[] args) {
		//请问spring中的prototype作用域是不是每次从spring获取都会创建一个新的对象.
		ClassA c1=new ClassA();//假设这个对象是spring帮你创建的
		beanPool.put("classA", c1);//假设这个为spring中作用为singleton的对象,对象还会存储到池
		//问题:对于c1指向的对象何为会被JVM认为是垃圾?(没有任何引用指向此对象)
		//当c1指向的对象也没有其它引用指向它,
		//此对象就为一个不可达对象了,也就是会被认为是一个垃圾对象了;
		c1=null;
		//beanPool.clear();
		//beanPool.remove("classA");
		//在JVM内存中对象的回收,需要通过GC系统去实现,GC系统的启动会分两种方式:
		//1)手动启动GC(一般不需要手动启动)
		System.gc();
		//2)自动启动GC(系统底层会随着创建对象的增加,然后基于内存情况,启动GC)
//		List<byte[]> list=new ArrayList<>();
//		for(int i=0;i<100000;i++) {
//			list.add(new byte[1024*1024]);
//		}
		//思考:
		//1.如何断定GC系统执行了?(通过配置JVM参数:-XX:+PrintGC)
		//2.如何断定对象被GC了?(可以通过重写Object类的finalize方法)
	}
}
