package com.cy.java.jvm;
class Point{}
public class TestClassLoader01 {
	public static void main(String[] args) {
		//思考:
		//1)请求在执行(Point p1)这个条语句时,是否会触发Point类的加载.
		//2)如何监控类是否被加载了(将类读到jvm内存):基于JVM参数配置-XX:+TraceClassLoading
		Point p1;//=new Point();
	}
}
